'use strict';

//Retrieves the list from Local storage
function get_todos () {
    var todos = new Array;
    var todos_storage = localStorage.getItem('todo');
    if (todos_storage !== null) {
        todos = JSON.parse(todos_storage);
    }
    return todos;
}

//Adds a new task to the list
function add_todos() {
    var task = document.getElementById('todo-task').value;
    var todos= get_todos();
    todos.push(task);
    localStorage.setItem('todo', JSON.stringify(todos));

    show_todolist();
}

//Removes a task from the list
function remove_todo() {
    var get_ID= this.getAttribute('Id');
    var todos= get_todos();
    todos.splice(get_ID,1);
    localStorage.setItem('todo', JSON.stringify(todos));

    show_todolist();
}

//Displays the list of ToDos
function show_todolist() {
    var todos= get_todos();

    var html='<ul>';
    for (var i=0; i<todos.length; i++) {
        html += '<li>' + todos[i] + '<button class="remove" id="' + i + '">Remove</button></li>';
    };
    html += '</ul>';

    document.getElementById('todos').innerHTML= html;

    var remove_buttons = document.getElementsByClassName('remove');
    for (var i=0; i < remove_buttons.length; i++) {
        remove_buttons[i].addEventListener('click', remove_todo);
    };
}

document.getElementById('add_todo').addEventListener('click', add_todos);

 show_todolist();

//Cancels user input
function cancel_user_input() {
    var cancelled_input = this.documentById('todo-task').reset;
}

document.getElementById('cancel').addEventListener('cliclk', cancel_user_input);

show_todolist();
